package briskula;

import briskula.models.Runda;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SerializationHandler {

    public static void serializeRunda(Runda runda, String path) {
        try (ObjectOutputStream stream = new ObjectOutputStream(
                new FileOutputStream(path))) {
            stream.writeObject(runda);
        } catch (IOException ex) {
            Logger.getLogger(SerializationHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Runda deserializeRunda(String path) {
        try (ObjectInputStream inStream = new ObjectInputStream(new FileInputStream(path))) {

            Object object = inStream.readObject();

            if (object instanceof Runda) {
                return (Runda) object;
            }
        } catch (IOException ex) {
            Logger.getLogger(SerializationHandler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SerializationHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
