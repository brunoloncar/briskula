package briskula.threads;

import javafx.application.Platform;
import briskula.models.Runda;
import briskula.scenes.PlayfieldController;

//ovaj thread provjerava jeli igra gotova
//igra je gotova ako je broj sakupljenih bodova oba igraca >= 120
//osvjezava se svaku sekundu a poziva se kada je igra inicijalizirana
public class ProvjeraJeliIgraGotovaThread implements Runnable {

    private final PlayfieldController pfc;
    private final Runda runda;

    public ProvjeraJeliIgraGotovaThread(PlayfieldController pfc) {
        this.pfc = pfc;
        this.runda = pfc.getRunda();
    }

    @Override
    public void run() {
//        System.out.println(runda.izbrojiPunteIgrac1());
        int suma = runda.izbrojiPunteIgrac1() + runda.izbrojiPunteIgrac2();
        if (suma >= 120) {
            Platform.runLater(()->pfc.krajRunde());
            try {
                synchronized (this){
                    wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
