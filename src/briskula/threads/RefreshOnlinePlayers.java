package briskula.threads;

import briskula.models.Korisnik;
import briskula.network.ClientCommunicator;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

public class RefreshOnlinePlayers extends Thread {

    private ObservableList<Korisnik> lista;
    private Korisnik korisnik;
    private final ClientCommunicator cc;

    public RefreshOnlinePlayers(ObservableList<Korisnik> korisnikObservableList, Korisnik korisnik, ClientCommunicator cc) {
        this.lista = korisnikObservableList;
        this.korisnik = korisnik;
        this.cc = cc;
    }

    @Override
    public void run() {
        System.out.println("refresh");
        //refreshing online players list
        ArrayList list = new ArrayList();
        cc.writeToServer(list);
        lista.setAll((ArrayList)cc.readFromServer());

    }


}
