package briskula.threads;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import briskula.models.Runda;
import briskula.scenes.PlayfieldController;

//ovaj thread osvjezava preostalo vrijeme unutar runde
//osvjezava se svaku sekundu a poziva se kada je igra inicijalizirana
public class PreostaloVrijemeThread implements Runnable {

    private final PlayfieldController pfc;

    public PreostaloVrijemeThread(PlayfieldController pfc) {
        this.pfc = pfc;
    }

    @Override
    public void run() {
        Runda runda = pfc.getRunda();

        //ako je runda pauzirana, pricekaj dok ne pocne
//        if(runda.isPauzirana() == true){
//            try {
//                synchronized (runda.getThreadLock()){
//                    //ako je igra zaustavljena, resetiraj preostalo vrijeme
//                    //i stavi thread u stanje cekanja
//                    runda.resetPreostaloVrijeme();
//                    runda.getThreadLock().wait();
//                }
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

        //dohvati preostalo vrijeme
        int preostaloVrijeme = runda.getPreostaloVrijeme();

        //ako ima jos vremena, smanji preostalo vrijeme za 1 sekundu
        if (preostaloVrijeme > 0) {
            //decreasanje vremena u ovoj lokalnoj varijabli
            preostaloVrijeme--;
            int finalPreostaloVrijeme = preostaloVrijeme;

            //postavi label o stanju vremena
            Platform.runLater(() -> pfc.getLblPreostaloVrijeme().setText(Integer.toString(finalPreostaloVrijeme)));

            //decreasanje vremena u objektu runde
            runda.decreasePreostaloVrijeme();
        }
        //inace zavrsi igru (gubitak) jer je isteklo vrijeme
        else {
            //pokazi popup samo ako je otvoren taj prozor
            if (pfc.getMojaKarta1().getScene().getWindow().isShowing()) {
                Platform.runLater(() -> {
                    //napravi alert i sakrij ekran
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Vrijeme isteklo");
                    alert.setHeaderText("Vrijeme isteklo");
                    alert.setContentText("Nažalost, vrijeme za igru je isteklo. Izgubili ste rundu.");
                    alert.showAndWait();
                    pfc.getMojaKarta1().getScene().getWindow().hide();
                });

                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
