package briskula.threads;

import javafx.application.Platform;
import javafx.scene.image.Image;
import briskula.models.Korisnik;
import briskula.scenes.PlayfieldController;

//ovaj thread se poziva kada je AI na redu za igranje
public class PlayAIThread extends Thread {

    private PlayfieldController pfc;

    public PlayAIThread(PlayfieldController pfc) {
        this.pfc = pfc;
    }

    public void run() {

        //ako je igra gotova ne bacaj nikakve karte vise
        if (pfc.getRunda().izbrojiPunteIgrac1() + pfc.getRunda().izbrojiPunteIgrac2() >= 120){
            return;
        }

        //prvo generiramo karte za AI ako ih nema
        pfc.getRunda().generirajKarteZaAIAkoIhNema();
        try {
            //pricekamo pola sekunde prije bacanja karte
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //te bacimo kartu i "dohvacamo pobjednika" makar
        // ako je AI prvi na redu mozda nije finalni pobjednik
        Korisnik winner = pfc.getRunda().baciKartuIVratiPobjednika(pfc.getRunda().getIgrac2(), pfc.getRunda().getIgrac2karta2()); //AI uvijek odabere kartu u sredini

        //radimo logiku ovisno o tome jeli prvi bacio kartu ili drugi
        if (!pfc.getOdigranaDonjaKarta().isVisible()) {
            //ako je 1. karta
            pfc.getOdigranaDonjaKarta().setImage(new Image(pfc.getRunda().getIgrac2karta2().getImgPath()));
            pfc.getOdigranaDonjaKarta().setVisible(true);

            //enableamo karte igraca (pritivnika od AI)
            pfc.enableMojeKarte();
        } else {
            //ako je 2. karta
            pfc.getOdigranaGornjaKarta().setImage(new Image(pfc.getRunda().getIgrac2karta2().getImgPath()));
            pfc.getOdigranaGornjaKarta().setVisible(true);

            //ako je AI pobjednik prikazi label o tome
            if (winner != null) {
                Korisnik finalWinner = winner;

                Platform.runLater(() -> pfc.setPobjednikTextLabel(finalWinner));

                //napravi thread koji će pokazati obavijest o pobjedniku i sakriti karte
                HideCardsThread hideCardsThread = new HideCardsThread(pfc);
                hideCardsThread.start();
            }
        }

    }
}
