package briskula.threads;

import briskula.network.ChatClient;
import briskula.network.ChatServer;

public class SendChatMessageThread extends Thread {

    private boolean amIServer;
    private String message;

    public SendChatMessageThread(boolean amIServer, String message) {
        this.amIServer = amIServer;
        this.message = message;
    }

    @Override
    public void run() {
        try {
            if (amIServer) {
                ChatServer.sendMessage(message);
            } else {
                ChatClient.sendMessage(message);
            }
        } catch (Exception e) {
        }
    }
}
