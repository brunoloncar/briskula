package briskula.threads;

import java.util.concurrent.ThreadFactory;

//ova klasa napravi daemon thread
//potrebno za timer.. inace on ostaje upaljen i nakon sto su
//svi windowsi zatvoreni
public class DaemonThreadFactory implements ThreadFactory {
    public Thread newThread(Runnable r) {
        Thread thread = new Thread(r);
        thread.setDaemon(true);
        return thread;
    }
}