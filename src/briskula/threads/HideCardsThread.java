package briskula.threads;

import javafx.application.Platform;
import briskula.scenes.PlayfieldController;


//ovaj thread se poziva svaki put kada su obe karte bacene
public class HideCardsThread extends Thread {

    private final PlayfieldController pfc;

    public HideCardsThread(PlayfieldController pfc) {
        this.pfc = pfc;
    }

    //ovaj thread se poziva kada je bačena druga karta
    //sa kratkim vremenskim odstojanjima se prikazuje label o pobjedniku ruke
    //a zatim se bacene karte sakriju kao i taj label
    public void run() {
        try {

            //pauziraj timer thread
            pfc.getRunda().setPauzirana(true);

            //prvo se priceka pola sekunde da se vidi koja je druga bacena karta
            Thread.sleep(500);

            //zatim se postavi tekst o pobjedniku na visible
            pfc.getPobjedaRuke().setVisible(true);

            //onda opet pricekamo da prode 0.75 sekundi..
            Thread.sleep(750);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //onda sakrijemo karte i taj tekst
        pfc.getOdigranaDonjaKarta().setVisible(false);
        pfc.getOdigranaGornjaKarta().setVisible(false);
        pfc.getPobjedaRuke().setVisible(false);

        //resetira preostalo vrijeme
        Platform.runLater(() -> {
            pfc.getRunda().resetPreostaloVrijeme();
            pfc.podijeliKartu();

        });


        //i ponovno pokrene timer
        pfc.getRunda().setPauzirana(false);
    }
}