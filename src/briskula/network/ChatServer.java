package briskula.network;

import javafx.scene.control.TextArea;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ChatServer {
    static Registry registry;

    public static boolean isServerRun() {
        return serverRun;
    }

    static boolean serverRun = false;

    public static void runServer(String nadimak, TextArea chatBox) throws Exception {
        ChatServiceImpl server = new ChatServiceImpl(nadimak, chatBox);
        registry = LocateRegistry.createRegistry(1099);

        ChatService stub = (ChatService) UnicastRemoteObject.exportObject(server, 0);
        registry.rebind("server", stub);
        System.out.println("Chat server pokrenut...");
        serverRun = true;
    }

    public static void sendMessage(String message) throws Exception {
        ChatService client;
        try {
            client = (ChatService) registry.lookup("client");
            if (client != null) {
                client.send(message);
            }
        } catch (NotBoundException ex) {

        } catch (AccessException ex) {
            Logger.getLogger(ChatServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        Thread.sleep(5000);
    }
}
