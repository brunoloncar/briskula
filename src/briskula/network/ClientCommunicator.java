package briskula.network;

import briskula.models.IgracNaRedu;
import briskula.models.Karta;
import briskula.models.Korisnik;
import briskula.models.Runda;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

public class ClientCommunicator {

    private Socket socket = null;
    private ObjectInputStream inputStream = null;
    private ObjectOutputStream outputStream = null;
    private boolean isConnected = false;
    private Korisnik korisnik;

    public ClientCommunicator(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public void writeToServer(Object object) {
        try {
            outputStream.writeObject(object);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Object readFromServer() {
        Object objectToBeRead = null;
        try {
            objectToBeRead = inputStream.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return objectToBeRead;
    }

    public void connectToServer() {
        while (!isConnected) {
            try {
                socket = new Socket("localhost", 12345);
                isConnected = true;
                outputStream = new ObjectOutputStream(socket.getOutputStream());
                outputStream.writeObject(korisnik);
                System.out.println("Connected");
                if (inputStream == null){
                    try {
                        inputStream = new ObjectInputStream(socket.getInputStream());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (SocketException se) {
                se.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
