package briskula.network;

import javafx.scene.control.TextArea;

import javax.xml.soap.Text;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChatClient {

    static Registry registry;

    public static void runClient(String nadimak, TextArea chatBox) throws Exception {
        ChatServiceImpl chat = new ChatServiceImpl(nadimak, chatBox);
        registry = LocateRegistry.getRegistry();

        ChatService stub = (ChatService) UnicastRemoteObject.exportObject(chat, 0);
        registry.rebind("client", stub);

        System.out.println("RMI klijent pokrenut.");
    }

    public static void sendMessage(String message) throws Exception {
        ChatService server;
        try {
            server = (ChatService) registry.lookup("server");
            if (server != null) {
                server.send(message);
            }
        } catch (NotBoundException ex) {
            ex.printStackTrace();
        } catch (AccessException ex) {
            ex.printStackTrace();
        }
    }
}
