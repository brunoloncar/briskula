package briskula.network;

import javax.naming.*;
import java.util.Hashtable;

public class JNDIConfig {
    Hashtable<String, String> hashtable;
    Context context;

    public JNDIConfig() throws NamingException {
        hashtable = new Hashtable<>();
        hashtable.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.fscontext.RefFSContextFactory");
        hashtable.put(Context.PROVIDER_URL, "file:saves");

        context = new InitialContext(hashtable);
    }

    public String getAllSaves() throws NamingException {
        StringBuilder sb = new StringBuilder();
        NamingEnumeration<NameClassPair> list = context.list("");
        while (list.hasMore()) {
            NameClassPair next = list.next();
            sb.append(next.getName() + '\n');
        }
        return sb.toString();
    }
}
