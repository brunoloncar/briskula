package briskula.network;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ChatService extends Remote {
    String getName() throws RemoteException;
    void send(String message) throws RemoteException;
}
