package briskula.network;

import javafx.scene.control.TextArea;

import javax.xml.soap.Text;
import java.rmi.RemoteException;

public class ChatServiceImpl implements ChatService {
    private final TextArea chatBox;
    private final String name;

    public ChatServiceImpl(String name, TextArea chatBox) throws RemoteException {
        this.name = name;
        this.chatBox = chatBox;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void send(String message) {
        chatBox.appendText(message + "\n");
    }
}
