package briskula.network;

import briskula.models.*;
import briskula.scenes.PlayfieldController;

import java.io.*;
import java.lang.reflect.Array;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Server {

    private ServerSocket serverSocket = null;
    private Socket socket1 = null;
    private Socket socket2 = null;
    private ObjectInputStream inStream1 = null;
    private ObjectInputStream inStream2 = null;
    private ObjectOutputStream outStream1 = null;
    private ObjectOutputStream outStream2 = null;
    private Korisnik igrac1 = null;
    private Korisnik igrac2 = null;
    private ArrayList<Korisnik> onlineKorisnici = new ArrayList<>();
    private Runda runda = null;
    private int statusIgre = 0; //0-nitko, 1-igrac1zeli, 2-igrac2zeli igra pocinje
    ArrayList<Potez> odigraniPotezi = new ArrayList<>();

    public Server() {

    }

    public static void main(String[] args) {
        new Server().communicate();
    }

    public void communicate() {
        try {
            //Pokretanje servera i cekanje igraca
            serverSocket = new ServerSocket(12345);
            System.out.println(String.format("Server pokrenut na portu: %d", serverSocket.getLocalPort()));
            System.out.println("Cekam spajanje igraca...");

            //Spajanje prvog igraca
            socket1 = serverSocket.accept();
            inStream1 = new ObjectInputStream(socket1.getInputStream());
            outStream1 = new ObjectOutputStream(socket1.getOutputStream());
            System.out.println("Igrac 1 spojen.");
            ReadIgracInputStream(inStream1);

            //Spajanje drugog igraca
            socket2 = serverSocket.accept();
            inStream2 = new ObjectInputStream(socket2.getInputStream());
            outStream2 = new ObjectOutputStream(socket2.getOutputStream());

            System.out.println("Igrac 2 spojen.");
            ReadIgracInputStream(inStream2);

            Thread t1 = new Thread(new ReadIgracInputThread(inStream1));
            Thread t2 = new Thread(new ReadIgracInputThread(inStream2));
            t1.start();
            t2.start();
//
//            do {
//                ReadIgracInputStream(inStream1);
//                ReadIgracInputStream(inStream2);
//            } while (true);

        } catch (SocketException se) {
            System.out.println(se);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException cn) {
            cn.printStackTrace();
        }
    }

    private void ReadIgracInputStream(ObjectInputStream inStream) throws IOException, ClassNotFoundException {
        if (inStream == null) {
            System.out.println("Greska prilikom citanja input streama.");
            return;
        }

        //citanje objekta
        try {
            Object objectToBeRead = inStream.readObject();

            if (objectToBeRead instanceof Korisnik) {
                Korisnik korisnik = (Korisnik) objectToBeRead;
                onlineKorisnici.add(korisnik);
                if (igrac1 == null) {
                    igrac1 = korisnik;
                    System.out.println("Igrac 1: " + korisnik.getNadimak());
                } else if (igrac2 == null) {
                    igrac2 = korisnik;
                    System.out.println("Igrac 2: " + korisnik.getNadimak());
                }
            } else if (objectToBeRead instanceof ArrayList) {
                if (outStream1 != null) {
                    System.out.println("Saljem listu online korisnika igracu 1.");
                    outStream1.writeObject(onlineKorisnici);
                }
                if (outStream2 != null) {
                    System.out.println("Saljem listu online korisnika igracu 2.");
                    outStream2.writeObject(onlineKorisnici);
                }
            } else if (objectToBeRead instanceof PlayfieldController) {
                switch (statusIgre) {
                    case 0: {
                        statusIgre = 1;
                        System.out.println("Zahtjev za igrom primljen -> igrac 1 spreman.");
                        break;
                    }
                    case 1: {
                        statusIgre = 2;
                        System.out.println("Zahtjev za igrom primljen -> igrac 2 spreman.");
                        System.out.println("Igra zapocinje...");

                        runda = new Runda(igrac1, igrac2);
                        if (outStream1 != null) {
                            System.out.println("Saljem rundu igracu 1: " + runda);
                            outStream1.writeObject(runda);
                        }
                        if (outStream2 != null) {
                            System.out.println("Saljem rundu igracu 2: " + runda);
                            outStream2.writeObject(runda);
                        }
                    }
                }
            } else if (objectToBeRead instanceof Runda) {
                if (inStream == inStream1) {
                    System.out.println("Saljem rundu igracu 1: " + runda);
                    outStream1.writeObject(runda);
                } else if (inStream == inStream2) {
                    System.out.println("Saljem rundu igracu 2: " + runda);
                    outStream2.writeObject(runda);
                }
            } else if (objectToBeRead instanceof Potez) {
                if (inStream == inStream1) {
                    System.out.println("Saljem sve poteze igracu 1.");
                    outStream1.writeObject(odigraniPotezi);
                } else if (inStream == inStream2) {
                    System.out.println("Saljem sve poteze igracu 1.");
                    outStream2.writeObject(odigraniPotezi);
                }
            } else if (objectToBeRead instanceof Karta) {
                Karta karta = (Karta) objectToBeRead;
                if (karta.getBroj() == -1) {
                    if (runda.getMac().size() < 1){
                        if (inStream == inStream1) {
                            System.out.println("Saljem kartu igracu 1: null");
                            outStream1.writeObject(new Karta(-1, Zog.Bastoni));
                        } else if (inStream == inStream2) {
                            System.out.println("Saljem kartu igracu 2: null");
                            outStream2.writeObject(new Karta(-1, Zog.Bastoni));
                        }
                        return;
                    }
                    karta = runda.getMac().pop();
                    System.out.println("povlacenje karte: " + karta);
                    if (inStream == inStream1) {
                        System.out.println("Saljem kartu igracu 1: " + karta);
                        outStream1.writeObject(karta);
                    } else if (inStream == inStream2) {
                        System.out.println("Saljem kartu igracu 2: " + karta);
                        outStream2.writeObject(karta);
                    }
                } else {
                    if (inStream == inStream1) {
                        outStream2.writeObject(karta);
                        System.out.println(igrac1.getNadimak() + " je bacio kartu " + karta.toString());
                        Korisnik k = runda.baciKartuIVratiPobjednika(igrac1, karta);
                        odigraniPotezi.add(new Potez(igrac1, karta));
                    } else {
                        outStream1.writeObject(karta);
                        System.out.println(igrac2.getNadimak() + " je bacio kartu " + karta.toString());
                        Korisnik k = runda.baciKartuIVratiPobjednika(igrac2, karta);
                        odigraniPotezi.add(new Potez(igrac2, karta));
                    }
                }
            } else if (objectToBeRead instanceof IgracNaRedu) {
                System.out.println(runda.getIgracNaRedu());
                if (inStream == inStream1) {
                    System.out.println("Saljem igraca na redu igracu 1.");
                    outStream1.writeObject(runda.getIgracNaRedu());
                } else if (inStream == inStream2) {
                    System.out.println("Saljem igraca na redu igracu 2.");
                    outStream2.writeObject(runda.getIgracNaRedu());
                }
            }
        } catch (EOFException e) {
            System.out.println("Input stream je zatvorio konekciju.");
            System.exit(0);
        }
    }

    private class ReadIgracInputThread implements Runnable {
        private final ObjectInputStream stream;

        public ReadIgracInputThread(ObjectInputStream stream) {
            this.stream = stream;
        }

        @Override
        public void run() {
            try {
                do {
                    ReadIgracInputStream(stream);
                } while (true);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}