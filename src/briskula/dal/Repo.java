package briskula.dal;

import briskula.models.Korisnik;

import java.util.*;

public class Repo {

    private static List<Korisnik> korisnici = new LinkedList<>();

    public static boolean AddKorisnik(Korisnik korisnik){
        for (Korisnik k : korisnici){
            if (k.getNadimak().equals(korisnik.getNadimak())){
                return false;
            }
        }
        korisnici.add(korisnik);
        return true;
    }

    public static Korisnik GetKorisnik(String nadimak){
        for (Korisnik korisnik : korisnici){
            if (korisnik.getNadimak().equals(nadimak)){
                return korisnik;
            }
        }
        return null;
    }

    public static Korisnik LoginAndGetUser(String nadimak, String lozinka){
        if (korisnici.size() == 0){
            korisnici.add(new Korisnik("adm", "brunoubica@gmail.com", "adm", 0, 0, "../../materials/img/user_images/user1.png" ));
            korisnici.add(new Korisnik("adm2", "brunoubica@gmail.com", "adm2", 0, 0, "../../materials/img/user_images/user2.png" ));
            korisnici.add(new Korisnik("AI", "brunoubicaa@gmail.com", "adm", 0, 0, "../../materials/img/user_images/user3.png" ));
        }

        for (Korisnik korisnik : korisnici){
            if (korisnik.getNadimak().equals(nadimak) && korisnik.getLozinka().equals(lozinka)){
                return korisnik;
            }
        }
        return null;
    }

    public static void PosaljiZaboravljenuLozinku(String email) {
        System.out.println("Poslana je lozinka na mail: " + email);
    }

    public static Korisnik GetKorisnik(UUID igracId) {
        for (Korisnik korisnik : korisnici) {
            if (korisnik.getId().equals(igracId)){
                return korisnik;
            }
        }
        return null;
    }

}
