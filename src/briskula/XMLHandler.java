package briskula;

import briskula.dal.Repo;
import briskula.models.Karta;
import briskula.models.Korisnik;
import briskula.models.Potez;
import briskula.models.Zog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class XMLHandler {
    public static void exportPotezi(ArrayList<Potez> potezi, String path) {
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("Runda");
            doc.appendChild(rootElement);

            potezi.forEach(potez -> {
                Element potezNode = doc.createElement("Potez");
                rootElement.appendChild(potezNode);

                //igrac
                Element igrac = doc.createElement("Igrac");
                igrac.appendChild(doc.createTextNode(potez.getIgrac().getNadimak()));
                potezNode.appendChild(igrac);

                //karta
                Element karta = doc.createElement("Karta");

                //broj
                Element broj = doc.createElement("Broj");
                broj.appendChild(doc.createTextNode(Integer.toString(potez.getKarta().getBroj())));
                karta.appendChild(broj);

                //zog
                Element zog = doc.createElement("Zog");
                zog.appendChild(doc.createTextNode(potez.getKarta().getZog().toString()));
                karta.appendChild(zog);

                potezNode.appendChild(karta);
            });

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(path));

            transformer.transform(source, result);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }

    }

    public static ArrayList<Potez> importPotezi(String path) {
        ArrayList<Potez> potezi = new ArrayList<>();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new File(path));
            Element root = doc.getDocumentElement();
            doc.getDocumentElement().normalize();
            NodeList poteziNodes = doc.getElementsByTagName("Potez");

            for (int temp = 0; temp < poteziNodes.getLength(); temp++) {
                Node nNode = poteziNodes.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element e = (Element) nNode;
                    Korisnik igrac = Repo.GetKorisnik(e.getElementsByTagName("Igrac").item(0).getTextContent());

                    Element kartaEl = (Element) e.getElementsByTagName("Karta").item(0);
                    int broj = Integer.parseInt(kartaEl.getElementsByTagName("Broj").item(0).getTextContent());
                    Zog zog = Zog.valueOf(kartaEl.getElementsByTagName("Zog").item(0).getTextContent());
                    Karta _karta = new Karta(broj, zog);
                    potezi.add(new Potez(igrac, _karta));
                }
            }
            return potezi;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
