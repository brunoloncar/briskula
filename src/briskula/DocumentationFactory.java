package briskula;

import briskula.dal.Repo;
import briskula.models.Karta;
import briskula.models.Korisnik;
import briskula.models.Runda;
import briskula.models.Zog;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DocumentationFactory {

    private static String pathName = "dokumentacija.html";

    public static String GenerateDocumentation() throws IOException {
        StringBuffer bafer = new StringBuffer();
        bafer.append("<!DOCTYPE html>\n<html>\n<head>\n<title>Briškula docs.</title>\n" +
                "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">" +
                "</head>\n"); //html begin file
        bafer.append("<body style=\"background-color: #ededed;\">\n");
        bafer.append("<div <body style=\"font-family: Menlo,Monaco,Consolas,monospace;background-color: #fff;width: 50%;margin: 10px auto;border: 1px solid rgba(0,0,0,0.4); padding: 15px;\">\n");
        File file = new File(pathName);
        FileWriter pisac = new FileWriter(file, false);

        bafer.append("<h1>Briškula documents - Popis klasa</h1><hr>\n");

        List<Class> classes = new ArrayList<>();
        classes.add(Runda.class);
        classes.add(Karta.class);
        classes.add(Zog.class);
        classes.add(Korisnik.class);
        classes.add(Repo.class);
        for (Class klasa : classes){

        bafer.append("<h2>" + klasa.getName() + "</h2>\n");
        bafer.append("<h3>Popis konstruktora</h3>\n");
        bafer.append("<table class=\"table table-bordered table-striped\" border='1'>\n");
        bafer.append("<th>Naziv konstruktora</th>"
                + "<th>Parametri konstruktora</th>");

        for(Constructor c : klasa.getConstructors()) {
            bafer.append("<tr><td>\n");
            bafer.append(c.getName());
            bafer.append("</td><td>\n");
            if(c.getParameterCount() > 0) {
                for(Parameter parameter : c.getParameters()) {
                    bafer.append(parameter.getType().getName() + " "
                            + parameter.getName() + "\n");

                }
            }
            bafer.append("</td></tr>\n");
        }

        bafer.append("</table>\n");
        bafer.append("<h3>Popis metoda</h3>\n");
        bafer.append("<table class=\"table table-bordered table-striped\" border='1'>\n");
        bafer.append("<th>Naziv metode</th>"
                + "<th>Ulazni parametri metode</th>"
                + "<th>Izlazni parametar metode</th>");

        for(Method metoda : klasa.getMethods()) {
            bafer.append("<tr><td>\n");
            bafer.append(metoda.getName());
            bafer.append("</td><td>\n");
            if(metoda.getParameterCount() > 0) {
                for(Parameter parameter : metoda.getParameters()) {
                    bafer.append(parameter.getType().getName() + " "
                            + parameter.getName() + "\n");

                }
            }
            bafer.append("</td><td>\n");
            bafer.append(metoda.getReturnType().getName() + "</td>");
        }

        bafer.append("</table>\n");

        bafer.append("<h3>Popis varijabli</h3>\n");
        bafer.append("<table class=\"table table-bordered table-striped\" border='1'>\n");
        bafer.append("<th>Naziv varijable</th>"
                + "<th>Access modifier</th>");

        for(Field polje : klasa.getDeclaredFields()) {
            String nazivVarijable = polje.getName();
            bafer.append("<tr><td>\n");
            bafer.append(nazivVarijable);
            bafer.append("</td><td>\n");
            bafer.append(generirajModifier(polje));
            bafer.append("</td></tr>\n");
        }
        bafer.append("</table><br>\n");
        bafer.append("<hr><br>\n");

        }
        bafer.append("<h5 style=\"text-align: center;\">Generirano: " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yy HH:mm")) + "</h5>");
        bafer.append("</div>\n</body>\n</html>\n"); //html end file
        pisac.append(bafer.toString());

        pisac.flush();
        pisac.close();
        return file.getAbsolutePath();
    }

    private static String generirajModifier(Field polje) {
        switch (polje.getModifiers()){
            case 2:
                return "private";
            case 0:
                return "public";
            default:
                return "unknown -> " + polje.getModifiers();
        }
    }
}
