package briskula.models;

import java.io.Serializable;
import java.util.UUID;

public class Korisnik implements Serializable {

    public Korisnik(){
        nadimak = null;
    }

    private UUID id;
    private String nadimak;
    private String email;

    public UUID getId() {
        return id;
    }

    private String lozinka;
    private int pobjede;
    private int porazi;

    public String getNadimak() {
        return nadimak;
    }

    public String getEmail() {
        return email;
    }

    public String getLozinka() {
        return lozinka;
    }

    public int getPobjede() {
        return pobjede;
    }

    public int getPorazi() {
        return porazi;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public Korisnik(String nadimak, String email, String lozinka, int pobjede, int porazi, String avatarURL) {
        this.id = UUID.randomUUID();
        this.nadimak = nadimak;
        this.email = email;
        this.lozinka = lozinka;
        this.pobjede = pobjede;
        this.porazi = porazi;
        this.avatarURL = avatarURL;
    }

    private String avatarURL;

    @Override
    public String toString() {
        return "Korisnik: " + nadimak;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Korisnik))
        {
            return false;
        }
        return ((Korisnik)obj).nadimak.equals(this.nadimak);
    }
}
