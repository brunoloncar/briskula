package briskula.models;

import java.io.Serializable;

public enum Zog implements Serializable {
    Bastoni,
    Dinari,
    Kupe,
    Spade;

    public static Zog parseZog(String letter) {
        switch (letter.toLowerCase()){
            case "b": return Bastoni;
            case "d": return Dinari;
            case "k": return Kupe;
            case "s": return Spade;
            default: return null;
        }
    }
}
