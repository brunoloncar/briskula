package briskula.models;


import java.io.Serializable;
public class Potez implements Serializable {
    private Korisnik igrac;
    private Karta karta;

    public Potez() {
    }

    public Potez(Korisnik igrac, Karta karta) {
        this.igrac = igrac;
        this.karta = karta;
    }

    public Korisnik getIgrac() {
        return igrac;
    }

    public Karta getKarta() {
        return karta;
    }

    @Override
    public String toString() {
        return String.format("Igrac: %s, karta: %s", igrac.getNadimak(), karta);
    }
}