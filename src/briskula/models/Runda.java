package briskula.models;

import javafx.scene.image.Image;
import briskula.scenes.PlayfieldController;

import java.io.Serializable;
import java.util.*;

public class Runda implements Serializable {

    //----------- Constructors -------------
    public Runda(){
    }

    public Runda(Korisnik igrac1, Korisnik igrac2) {
        idRunda = UUID.randomUUID();
        this.igrac1 = igrac1;
        this.igrac2 = igrac2;
        zavrsena = false;
        igrac1osvojeno = new Stack<>();
        igrac2osvojeno = new Stack<>();
        mac = dohvatiNoviPromijesaniMac();
        igracNaRedu = igrac1;
        deserialized = false;
    }
    //----------- Constructors -------------

    //----------- Variables, setters and getters -------------
    private UUID idRunda;
    private Stack<Karta> mac;
    private boolean zavrsena;
    private Zog adut;

    private int preostaloVrijeme = 31;
    public int getPreostaloVrijeme() {
        return preostaloVrijeme;
    }
    public void setPreostaloVrijeme(int preostaloVrijeme) {
        this.preostaloVrijeme = preostaloVrijeme;
    }

    private boolean pauzirana;
    public boolean isPauzirana() {
        return pauzirana;
    }

//    private Serializable threadLock = new Serializable();
//    public Serializable getThreadLock() {
//        return threadLock;
//    }

    public void setPauzirana(boolean pauzirana) {
        this.pauzirana = pauzirana;
    }

    //igraci
    private Korisnik igrac1;
    private Korisnik igrac2;

    public Korisnik getIgrac1() {
        return igrac1;
    }

    public Korisnik getIgrac2() {
        return igrac2;
    }

    private Korisnik igracNaRedu;

    public Korisnik getIgracNaRedu() {
        return igracNaRedu;
    }


    private boolean deserialized;

    public boolean isDeserialized() {
        return deserialized;
    }

    public void setDeserialized(boolean deserialized) {
        this.deserialized = deserialized;
    }

    //karte
    private Stack<Karta> igrac1osvojeno;
    private Stack<Karta> igrac2osvojeno;
    public Stack<Karta> getIgrac1osvojeno() {
        return igrac1osvojeno;
    }
    public void setIgrac1osvojeno(Stack<Karta> igrac1osvojeno) {
        this.igrac1osvojeno = igrac1osvojeno;
    }
    public Stack<Karta> getIgrac2osvojeno() {
        return igrac2osvojeno;
    }
    public void setIgrac2osvojeno(Stack<Karta> igrac2osvojeno) {
        this.igrac2osvojeno = igrac2osvojeno;
    }

    private Karta odigranaDonjaKarta;
    private Karta odigranaGornjaKarta;

    public Karta getOdigranaDonjaKarta() {
        return odigranaDonjaKarta;
    }

    public void setOdigranaDonjaKarta(Karta odigranaDonjaKarta) {
        this.odigranaDonjaKarta = odigranaDonjaKarta;
    }

    public Karta getOdigranaGornjaKarta() {
        return odigranaGornjaKarta;
    }

    public void setOdigranaGornjaKarta(Karta odigranaGornjaKarta) {
        this.odigranaGornjaKarta = odigranaGornjaKarta;
    }

    private Karta igrac1karta1;
    private Karta igrac1karta2;
    private Karta igrac1karta3;
    private Karta igrac2karta1;
    private Karta igrac2karta2;
    private Karta igrac2karta3;

    public Karta getIgrac1karta1() {
        return igrac1karta1;
    }

    public Karta getIgrac1karta2() {
        return igrac1karta2;
    }

    public Karta getIgrac1karta3() {
        return igrac1karta3;
    }

    public Karta getIgrac2karta1() {
        return igrac2karta1;
    }

    public Karta getIgrac2karta2() {
        return igrac2karta2;
    }

    public Karta getIgrac2karta3() {
        return igrac2karta3;
    }


    //controlleri
    private transient PlayfieldController igrac1Controller;
    private transient PlayfieldController igrac2Controller;

    public PlayfieldController getIgrac1Controller() {
        return igrac1Controller;
    }

    public PlayfieldController getIgrac2Controller() {
        return igrac2Controller;
    }

    public void setIgrac1Controller(PlayfieldController igrac1Controller) {
        this.igrac1Controller = igrac1Controller;
    }

    public void setIgrac2Controller(PlayfieldController igrac2Controller) {
        this.igrac2Controller = igrac2Controller;
    }
    //----------- Variables, setters and getters -------------


    //----------- Methods -------------
    public Stack<Karta> getMac() {
        return mac;
    }

    private Stack<Karta> dohvatiNoviPromijesaniMac() {
        Stack<Karta> mac = new Stack<>();
        for (int i = 0; i < 4; i++) {
            Zog zog = null;
            switch (i) {
                case 0:
                    zog = Zog.Bastoni;
                    break;
                case 1:
                    zog = Zog.Dinari;
                    break;
                case 2:
                    zog = Zog.Kupe;
                    break;
                case 3:
                    zog = Zog.Spade;
                    break;
            }
            for (int j = 1; j < 14; j++) {
                if (j < 8 || j > 10) {
                    Karta karta = new Karta(j, zog);
                    mac.add(karta);
                }
            }
        }
        Collections.shuffle(mac);
        return mac;
    }

    public Image getAdut() {
        return new Image(mac.firstElement().getImgPath());
    }

    private Zog getAdutZog() {
        if (adut == null){
            adut = mac.firstElement().getZog();
        }
        return adut;
    }

    public int izbrojiPunte(Stack<Karta> igracOsvojeno) {
        int ukupno = 0;
        for (Karta karta : igracOsvojeno) {
            ukupno += karta.getVrijednost();
        }
        return ukupno;
    }

    public int izbrojiPunteIgrac1() {
        int ukupno = 0;
            for (Karta karta : igrac1osvojeno) {
            ukupno += karta.getVrijednost();
        }
        return ukupno;
    }

    public int izbrojiPunteIgrac2() {
        int ukupno = 0;
        for (Karta karta : igrac2osvojeno) {
            ukupno += karta.getVrijednost();
        }
        return ukupno;
    }


    private Karta getKartaZaKorisnika(Korisnik korisnik, int karta) {
        if (korisnik == igrac1) {
            switch (karta) {
                case 1:
                    return igrac1karta1;
                case 2:
                    return igrac1karta2;
                case 3:
                    return igrac1karta3;
            }
        } else {
            switch (karta) {
                case 1:
                    return igrac2karta1;
                case 2:
                    return igrac2karta2;
                case 3:
                    return igrac2karta3;
            }
        }
        return null;
    }

    public Image getMojaKarta(Korisnik korisnik, int i) {

        //ako je prva runda makni prve 3 karte za korisnika
        if (korisnik == getIgrac2() && mac.size() == 60){
            for (int y = 3;y<3; y++){
                getMac().pop();
            }
        }

        Karta karta = mac.pop();

        if (korisnik == igrac1) {
            switch (i) {
                case 1:
                    igrac1karta1 = karta;
                    break;
                case 2:
                    igrac1karta2 = karta;
                    break;
                case 3:
                    igrac1karta3 = karta;
                    break;
            }
        } else {
            switch (i) {
                case 1:
                    igrac2karta1 = karta;
                    break;
                case 2:
                    igrac2karta2 = karta;
                    break;
                case 3:
                    igrac2karta3 = karta;
                    break;
            }
        }
        return new Image(karta.getImgPath());
    }

    public Korisnik baciKartuIVratiPobjednika(Korisnik korisnik, Karta karta) { //vraca true ako je prosljedeni korisnik pobjednik
        if (odigranaDonjaKarta == null) {
            //ako nije odigrana donja karta, stavi ovu na donju
            odigranaDonjaKarta = karta;
            //vrati da jos nema pobjednika jer je tek prva karta bačena
            return null;
        } else {
            odigranaGornjaKarta = karta;
            if (PrvaJacaOdDruge(odigranaDonjaKarta, odigranaGornjaKarta)) {
                igracNaRedu = GetProtivnik(korisnik);
                dajKartePobjedniku(odigranaDonjaKarta, odigranaGornjaKarta, igracNaRedu);
                odigranaDonjaKarta = null;
                odigranaGornjaKarta = null;
                return igracNaRedu;
            } else {
                igracNaRedu = korisnik;
                dajKartePobjedniku(odigranaDonjaKarta, odigranaGornjaKarta, igracNaRedu);
                odigranaDonjaKarta = null;
                odigranaGornjaKarta = null;
                return igracNaRedu;
            }
        }
    }

    private void dajKartePobjedniku(Karta odigranaDonjaKarta, Karta odigranaGornjaKarta, Korisnik igracNaRedu) {
        if (igracNaRedu.getNadimak() == igrac1.getNadimak()){
            igrac1osvojeno.add(odigranaDonjaKarta);
            igrac1osvojeno.add(odigranaGornjaKarta);
        }
        else {
            igrac2osvojeno.add(odigranaDonjaKarta);
            igrac2osvojeno.add(odigranaGornjaKarta);
        }
    }

    private boolean PrvaJacaOdDruge(Karta odigranaDonjaKarta, Karta odigranaGornjaKarta) {
        //ako drugi nije odgovarao i nije bacio zog
        if (!odigranaGornjaKarta.getZog().equals(odigranaDonjaKarta.getZog()) &&
                !odigranaGornjaKarta.getZog().equals(getAdutZog())) {
            return true;
        }

        //ako je drugi odgovarao
        if (odigranaGornjaKarta.getZog().equals(odigranaDonjaKarta.getZog())) {
            return odigranaDonjaKarta.getJacina() > odigranaGornjaKarta.getJacina();
        }

        //ako je drugi bacio zog
        if (!odigranaDonjaKarta.getZog().equals(getAdutZog()) && odigranaGornjaKarta.getZog().equals(getAdutZog())) {
            return false;
        } else {
            return true;
        }
    }

    private Korisnik GetProtivnik(Korisnik korisnik) {
        if (korisnik == igrac1) {
            return igrac2;
        } else {
            return igrac1;
        }
    }

    public Korisnik getPobjednik() {
        int puntiIgraca1 = izbrojiPunte(igrac1osvojeno);
        int puntiIgraca2 = izbrojiPunte(igrac2osvojeno);

        if (puntiIgraca1 > puntiIgraca2) {
            return igrac1;
        } else if (puntiIgraca2 > puntiIgraca1) {
            return igrac2;
        } else {
            return null;
        }
    }

    public boolean isProtivnikAI() {
        return igrac2.getNadimak().equals("AI");
    }

    public void generirajKarteZaAIAkoIhNema() {
        if (igrac2karta1 == null && igrac2karta2 == null && igrac2karta3 == null && isProtivnikAI()) {
            igrac2karta1 = mac.pop();
            igrac2karta2 = mac.pop();
            igrac2karta3 = mac.pop();
        }
    }

    public void vuciZaAI() {
        if (isProtivnikAI())
            if (mac.size() != 0){
                igrac2karta2 = mac.pop();
            }
            else {
                if (igrac2karta1 != null){
                    igrac2karta2 = igrac2karta1;
                    igrac2karta1 = null;
                }
                else if (igrac2karta3 != null) {
                    igrac2karta2 = igrac2karta3;
                    igrac2karta3 = null;
                }
            }
    }

    public void decreasePreostaloVrijeme() {
        preostaloVrijeme--;
    }
    public void resetPreostaloVrijeme() {
        preostaloVrijeme = 31;
    }
    //----------- Methods -------------

}
