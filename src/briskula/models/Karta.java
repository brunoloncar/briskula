package briskula.models;

import java.io.Serializable;

public class Karta implements Serializable {

    //Privates
    private int broj;
    private Zog zog;


    //Ctors
    public Karta(int broj, Zog zog) {
        this.broj = broj;
        this.zog = zog;
    }

    public static Karta parse(String imgPath) {
        imgPath = imgPath.substring(imgPath.lastIndexOf('/')+1, imgPath.lastIndexOf('.'));
        String lines[] = imgPath.split("-");
        return new Karta(Integer.parseInt(lines[0]), Zog.parseZog(lines[1]));
    }

    //Getters and setters
    public int getBroj() {
        return broj;
    }

    public void setBroj(int broj) {
        this.broj = broj;
    }

    public Zog getZog() {
        return zog;
    }

    public void setZog(Zog zog) {
        this.zog = zog;
    }


    //Card specific values
    public String getImgPath() {
        return getClass().getResource(String.format("../../materials/img/karte/%d-%s.png",
                broj, zog.name().toLowerCase().charAt(0))).toExternalForm();
    }

    public int getVrijednost() {
        switch (broj) {
            case 1:
                return 11;
            case 3:
                return 10;
            case 13:
                return 4;
            case 12:
                return 3;
            case 11:
                return 2;
            default:
                return 0;
        }
    }

    //Overrides
    @Override
    public String toString() {
        return String.format("[%d, %s]", broj, zog);
    }

    public int getJacina() {
        switch (broj){
            case 2: return 1;
            case 4: return 2;
            case 5: return 3;
            case 6: return 4;
            case 7: return 5;
            case 11: return 6;
            case 12: return 7;
            case 13: return 8;
            case 3: return 9;
            case 1: return 10;
            default: return 0;
        }
    }
}
