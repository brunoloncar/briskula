package briskula.scenes;

import briskula.models.Karta;
import javafx.scene.image.Image;

import java.net.URL;

class CardImage extends Image {

    private final Karta karta;
    private final String url;

    public CardImage(String url, Karta karta) {
        super(url);
        this.url = url;
        this.karta = karta;
    }

    public String getURL() {
        return url;
    }

    public Karta getKarta() {
        return karta;
    }

}
