package briskula.scenes;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import briskula.XMLHandler;
import briskula.models.Potez;
import briskula.network.ClientCommunicator;
import briskula.network.JNDIConfig;
import briskula.threads.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableListBase;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import briskula.SerializationHandler;
import briskula.dal.Repo;
import briskula.models.Korisnik;
import briskula.models.Runda;

import javax.naming.NamingException;


public class PocetnaStranicaController implements Initializable {

    //----------- Variables, setters and getters -------------
    private Korisnik korisnik;
    ClientCommunicator cc;


    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    ObservableList<Korisnik> korisnikObservableList = FXCollections.observableArrayList();
    //----------- Variables, setters and getters -------------


    //----------- FXML nodes -------------
    @FXML
    private BorderPane pocetnaStranicaBody;

    @FXML
    private Button btnIgrajOnline;

    @FXML
    private Label lblVaseIme;

    @FXML
    private Label lblVasWL;

    @FXML
    private ImageView imgVasAvatar;

    @FXML
    private BorderPane playfield;

    @FXML
    private TableView tableIgraciOnline;
    //----------- FXML nodes -------------


    //----------- Actions -------------
    @FXML
    private void btnOdjavaClick(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("Login.fxml"));
            Scene scene = new Scene(root);

            Stage stage = new Stage();
            Image applicationIcon = new Image(getClass().getResourceAsStream("../../materials/img/favicon.png"));
            stage.getIcons().add(applicationIcon);
            stage.setTitle("Briškula");
            stage.setScene(scene);
            stage.show();

            ((Node) (event.getSource())).getScene().getWindow().hide();
        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void btnPostavkeClick(ActionEvent event) {
        korisnikObservableList.add(Repo.GetKorisnik("adm"));
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Postavke.fxml"));
            VBox root = loader.load();
            PostavkeController pc = loader.getController();
            pc.initData(korisnik);

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            Image applicationIcon = new Image(getClass().getResourceAsStream("../../materials/img/favicon.png"));
            stage.getIcons().add(applicationIcon);
            stage.setTitle("Uređivanje podataka");

            stage.setScene(scene);
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void btnSpremljeneIgre(ActionEvent event) {
        JNDIConfig jndiConfig = null;
        try {
            jndiConfig = new JNDIConfig();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Spremljene igre");
            alert.setHeaderText("Spremljene igre");
            alert.setContentText(jndiConfig.getAllSaves());
            alert.showAndWait();
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void kakoIgratiClick(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("KakoIgrati.fxml"));
            Scene scene = new Scene(root);

            Stage stage = new Stage();
            Image applicationIcon = new Image(getClass().getResourceAsStream("../../materials/img/favicon.png"));
            stage.getIcons().add(applicationIcon);
            stage.setTitle("Kako igrati");
            stage.setScene(scene);
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void rezultatiClick(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("Rezultati.fxml"));
            Scene scene = new Scene(root);

            Stage stage = new Stage();
            Image applicationIcon = new Image(getClass().getResourceAsStream("../../materials/img/favicon.png"));
            stage.getIcons().add(applicationIcon);
            stage.setTitle("Rezultati");
            stage.setScene(scene);
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void brzaIgraClick(ActionEvent event) {
        Runda runda = new Runda(korisnik, Repo.GetKorisnik("AI"));
        OpenPlayfield(runda, runda.getIgrac1(), 0);
    }

    @FXML
    private void igrajOnlineClick(ActionEvent event) {

        Parent root;
        PlayfieldController playfieldController = null;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Playfield.fxml"));
            root = loader.load();
            playfieldController = loader.getController();

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            Image applicationIcon = new Image(getClass().getResourceAsStream("../../materials/img/favicon.png"));
            stage.getIcons().add(applicationIcon);
            stage.setTitle("Briškula - u igri ...");

//            Alert alert = new Alert(Alert.AlertType.INFORMATION);
//            alert.setHeaderText("Cekanje drugog igraca");
//            alert.setHeaderText("Cekanje drugog igraca");
//            alert.setContentText("Molimo pricekajte dok se ne spoji drugi igrac.");
//            alert.show();
            stage.setScene(scene);
            cc.writeToServer(new PlayfieldController());
            Thread t1 = new Thread(new LoadingThread(cc, stage, playfieldController));
            t1.setDaemon(true);
            t1.start();

        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private class LoadingThread implements Runnable {
        private final ClientCommunicator cc;
        private final Stage stage;
        private final PlayfieldController playfieldController;

        public LoadingThread(ClientCommunicator cc, Stage stage, PlayfieldController playfieldController) {
            this.cc = cc;
            this.stage = stage;
            this.playfieldController = playfieldController;
        }

        @Override
        public void run() {
            Runda dummyRunda = new Runda();
            while (cc.readFromServer() != null) {
                Platform.runLater(() -> {
                    cc.writeToServer(dummyRunda);
                    Runda runda = (Runda)cc.readFromServer();
                    playfieldController.setAndPrepareRunda(runda, korisnik, cc);
                    stage.show();
                });
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @FXML
    private void btnUcitajIgru(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("saves"));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Spremljene igre", "*.si"));
        File file = fileChooser.showOpenDialog(pocetnaStranicaBody.getScene().getWindow());

        Runda runda = SerializationHandler.deserializeRunda(file.getAbsolutePath());
        runda.setDeserialized(true);
        if (runda != null) {
            OpenPlayfield(runda, runda.getIgrac1(), 0);
        }
    }

    @FXML
    private void btnReplay(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("xmls"));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("XML potezi", "*.xml"));
        File file = fileChooser.showOpenDialog(pocetnaStranicaBody.getScene().getWindow());
        if (file == null){
            return;
        }

        ArrayList<Potez> potezi = XMLHandler.importPotezi(file.getAbsolutePath());
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Playfield.fxml"));
            root = loader.load();
            PlayfieldController playfieldController = loader.getController();

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            Image applicationIcon = new Image(getClass().getResourceAsStream("../../materials/img/favicon.png"));
            stage.getIcons().add(applicationIcon);
            stage.setTitle("Briškula - u igri ...");

            stage.setScene(scene);
            stage.setHeight(stage.getHeight() - stage.getHeight()*0.15);
            playfieldController.setPotezi(potezi);
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void prihvatiClick(ActionEvent event) {

    }

    //----------- Actions -------------


    //----------- Methods -------------
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    public void initData(Korisnik korisnik) {
        this.korisnik = korisnik;
        if (korisnik != null) {
            lblVaseIme.setText("Pozdrav, " + korisnik.getNadimak() + ".");
            lblVasWL.setText(String.format("W: %d / L: %d", korisnik.getPobjede(), korisnik.getPorazi()));
            imgVasAvatar.setImage(new Image(getClass().getResourceAsStream(korisnik.getAvatarURL())));
            loadTablicaOnlineIgraca();
            cc = new ClientCommunicator(korisnik);
            cc.connectToServer();
        }
    }

    public void OpenPlayfield(Runda runda, Korisnik zaOvogKorisnika, double xodmak) {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Playfield.fxml"));
            root = loader.load();
            PlayfieldController playfieldController = loader.getController();
//            playfieldController.setAndPrepareRunda(runda, zaOvogKorisnika);

            Scene scene = new Scene(root);
            Stage stage = new Stage();

            Image applicationIcon = new Image(getClass().getResourceAsStream("../../materials/img/favicon.png"));
            stage.getIcons().add(applicationIcon);
            stage.setTitle("Briškula - u igri ...");

            stage.setScene(scene);
            if (xodmak != 0) {
                stage.setX(xodmak); //zakomentirati u multiplayeru
            }
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadTablicaOnlineIgraca() {
        TableColumn colIgraciNaMrezi = new TableColumn("Igrači na mreži");
        TableColumn colPobjede = new TableColumn("Pobjede");
        TableColumn colPorazi = new TableColumn("Porazi");

        colIgraciNaMrezi.setCellValueFactory(new PropertyValueFactory<Korisnik, String>("nadimak"));
        colPobjede.setCellValueFactory(new PropertyValueFactory<Korisnik, String>("pobjede"));
        colPorazi.setCellValueFactory(new PropertyValueFactory<Korisnik, String>("porazi"));

        colIgraciNaMrezi.prefWidthProperty().bind(tableIgraciOnline.widthProperty().divide(2));
        colPobjede.prefWidthProperty().bind(tableIgraciOnline.widthProperty().divide(4));
        colPorazi.prefWidthProperty().bind(tableIgraciOnline.widthProperty().divide(4).add(-2));
        colIgraciNaMrezi.setStyle("-fx-alignment: CENTER;");
        colPobjede.setStyle("-fx-alignment: CENTER;");
        colPorazi.setStyle("-fx-alignment: CENTER;");

        korisnikObservableList.add(korisnik);
        tableIgraciOnline.setFocusTraversable(false);
        tableIgraciOnline.setItems(korisnikObservableList);
        tableIgraciOnline.getColumns().setAll(colIgraciNaMrezi, colPobjede, colPorazi);


        MenuItem mi2 = new MenuItem("Igraj");
        MenuItem mi1 = new MenuItem("Chat");
        mi1.setOnAction((ActionEvent event) -> {
            Korisnik protivnik = (Korisnik) tableIgraciOnline.getSelectionModel().getSelectedItem();
            if (protivnik.getNadimak().equals(korisnik.getNadimak())) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Greska");
                alert.setHeaderText("Greska");
                alert.setContentText("Ne mozete pokrenuti chat sami sa sobom...");
                alert.showAndWait();
                return;
            }
            Parent root;
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("Chat.fxml"));
                root = loader.load();
                ChatController cc = loader.getController();
                cc.initData(korisnik, protivnik);

                Stage stage = new Stage();
                Scene scene = new Scene(root);

                Image applicationIcon = new Image(ScenesHelper.class.getResourceAsStream("../../materials/img/favicon.png"));
                stage.getIcons().add(applicationIcon);
                stage.setTitle("Chat");
                stage.setScene(scene);
                stage.show();

            } catch (IOException ex) {
                Logger.getLogger(ScenesHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        ContextMenu menu = new ContextMenu();
        menu.getItems().add(mi2);
        menu.getItems().add(mi1);
        tableIgraciOnline.setContextMenu(menu);

//        DaemonThreadFactory dtf = new DaemonThreadFactory();
//        ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor(dtf);
//        exec.scheduleAtFixedRate(new RefreshOnlinePlayers(korisnikObservableList, korisnik, cc), 0, 1, TimeUnit.SECONDS);

    }

    public Button getBtnIgrajOnline() {
        return btnIgrajOnline;
    }

    public void setBtnIgrajOnline(Button btnIgrajOnline) {
        this.btnIgrajOnline = btnIgrajOnline;
    }


    //----------- Methods -------------

}
