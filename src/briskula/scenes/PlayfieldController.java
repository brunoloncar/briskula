package briskula.scenes;

import briskula.XMLHandler;
import briskula.models.*;
import briskula.network.ClientCommunicator;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import briskula.SerializationHandler;
import briskula.dal.Repo;
import briskula.threads.*;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


public class PlayfieldController implements Initializable, Serializable {

    public ArrayList<Potez> potezi;
    //----------- Variables, setters and getters -------------
    private Korisnik korisnik;
    private ClientCommunicator cc;

    public Korisnik getKorisnik() {
        return korisnik;
    }


    private Runda runda;

    public Runda getRunda() {
        return runda;
    }

    public void setRunda(Runda runda) {
        this.runda = runda;
    }


    public ImageView getMojaKarta1() {
        return mojaKarta1;
    }

    public void setMojaKarta1(ImageView mojaKarta1) {
        this.mojaKarta1 = mojaKarta1;
    }

    public ImageView getMojaKarta2() {
        return mojaKarta2;
    }

    public void setMojaKarta2(ImageView mojaKarta2) {
        this.mojaKarta2 = mojaKarta2;
    }

    public ImageView getMojaKarta3() {
        return mojaKarta3;
    }

    public void setMojaKarta3(ImageView mojaKarta3) {
        this.mojaKarta3 = mojaKarta3;
    }

    public void setOdigranaGornjaKarta(ImageView odigranaGornjaKarta) {
        this.odigranaGornjaKarta = odigranaGornjaKarta;
    }

    public void setOdigranaDonjaKarta(ImageView odigranaDonjaKarta) {
        this.odigranaDonjaKarta = odigranaDonjaKarta;
    }

    public void setPobjedaRuke(Label pobjedaRuke) {
        this.pobjedaRuke = pobjedaRuke;
    }

    //----------- Variables, setters and getters -------------


    //----------- FXML nodes -------------
    @FXML
    private BorderPane rootBorder;

    @FXML
    private Button btnZapocniIgru;

    @FXML
    private ImageView mojaKarta1;

    @FXML
    private ImageView mojaKarta2;

    @FXML
    private ImageView mojaKarta3;

    @FXML
    private ImageView odigranaGornjaKarta;

    public ImageView getOdigranaGornjaKarta() {
        return odigranaGornjaKarta;
    }

    @FXML
    private ImageView odigranaDonjaKarta;

    public ImageView getOdigranaDonjaKarta() {
        return odigranaDonjaKarta;
    }

    @FXML
    private ImageView adut;

    @FXML
    private ImageView mac;

    @FXML
    private Label lblPreostaloVrijeme;

    @FXML
    private Label lblProtivnik;

    public Label getLblPreostaloVrijeme() {
        return lblPreostaloVrijeme;
    }

    public void setLblPreostaloVrijeme(Label lblPreostaloVrijeme) {
        this.lblPreostaloVrijeme = lblPreostaloVrijeme;
    }

    @FXML
    private Label pobjedaRuke;

    public Label getPobjedaRuke() {
        return pobjedaRuke;
    }

    //----------- FXML nodes -------------


    //----------- Actions -------------
    @FXML
    public void mojaKartaClick(MouseEvent event) {
        doKartaClick(event);
    }

    @FXML
    private void predajaClick(ActionEvent event) {
        cc.writeToServer(new ArrayList<Korisnik>());
//        ScenesHelper.CreateAndHideOldScreen("KrajRunde.fxml", "Kraj runde", event);
    }

    @FXML
    private void spremiClick(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("saves"));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Spremljene igre", "*.si"));
        File file = fileChooser.showSaveDialog(rootBorder.getScene().getWindow());
        if (file != null) {
            SerializationHandler.serializeRunda(runda, file.getAbsolutePath());
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Spremljeno");
            alert.setHeaderText("Spremljeno");
            alert.setContentText("Igra je uspješno spremljena.");
            alert.showAndWait();
        }
    }

    @FXML
    private void xmlexportClick(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("xmls"));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("XML potezi", "*.xml"));
        File file = fileChooser.showSaveDialog(rootBorder.getScene().getWindow());
        if (file != null) {
            cc.writeToServer(new Potez());
            ArrayList<Potez> potezi = (ArrayList<Potez>) cc.readFromServer();
            XMLHandler.exportPotezi(potezi, file.getAbsolutePath());
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Spremljeno");
            alert.setHeaderText("Spremljeno");
            alert.setContentText("Potezi su uspješno spremljeni.");
            alert.showAndWait();
        }
    }

    @FXML
    private void chatClick(ActionEvent event) {
        Parent root;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Chat.fxml"));
            root = loader.load();
            ChatController cc = loader.getController();
            if (korisnik == runda.getIgrac1()) {
                cc.initData(korisnik, runda.getIgrac2());
            } else {
                cc.initData(runda.getIgrac2(), korisnik);
            }

            Stage stage = new Stage();
            Scene scene = new Scene(root);

            Image applicationIcon = new Image(ScenesHelper.class.getResourceAsStream("../../materials/img/favicon.png"));
            stage.getIcons().add(applicationIcon);
            stage.setTitle("Chat");
            stage.setScene(scene);
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(ScenesHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //----------- Actions -------------


    //----------------[BACANJE KARTE]----------------
    public void setAndPrepareRunda(Runda runda, Korisnik korisnik, ClientCommunicator cc) {
        this.runda = runda;
        this.korisnik = korisnik;
        this.cc = cc;

        if (korisnik == runda.getIgrac1()){
            lblProtivnik.setText(runda.getIgrac2().getNadimak());
        } else {
            lblProtivnik.setText(runda.getIgrac1().getNadimak());
        }

        adut.setImage(runda.getAdut());

        //sakrivamo sve neodigrane karte
        odigranaGornjaKarta.setVisible(false);
        odigranaDonjaKarta.setVisible(false);
        pobjedaRuke.setVisible(false);

        Karta _k = vuciKartu();
        mojaKarta1.setImage(new CardImage(_k.getImgPath(), _k));

        _k = vuciKartu();
        mojaKarta2.setImage(new CardImage(_k.getImgPath(), _k));

        _k = vuciKartu();
        mojaKarta3.setImage(new CardImage(_k.getImgPath(), _k));

        //        if (runda.isDeserialized()) {
//            //ako je deserijalizirana, dohvacamo karte za igrace
//            if (korisnik == runda.getIgrac1()) {
//                //ako je igrac1
//                mojaKarta1.setImage(new Image(runda.getIgrac1karta1().getImgPath()));
//                mojaKarta2.setImage(new Image(runda.getIgrac1karta2().getImgPath()));
//                mojaKarta3.setImage(new Image(runda.getIgrac1karta3().getImgPath()));
//            } else {
//                //ako je igrac2
//                mojaKarta1.setImage(new Image(runda.getIgrac2karta1().getImgPath()));
//                mojaKarta2.setImage(new Image(runda.getIgrac2karta2().getImgPath()));
//                mojaKarta3.setImage(new Image(runda.getIgrac2karta3().getImgPath()));
//            }
//            runda.setDeserialized(false);
//        } else {

        //inace dohvaćamo nove karte za igraca
//            mojaKarta1.setImage(getRunda().getMojaKarta(korisnik, 1));
//            mojaKarta2.setImage(getRunda().getMojaKarta(korisnik, 2));
//            mojaKarta3.setImage(getRunda().getMojaKarta(korisnik, 3));
//        }

        //disableamo karte za igrača ako nije prvi na redu

        //tko je na redu?
        disableMojeKarte();
        pokreniThreadove();
        startajRuku();

    }

    public void setPotezi(ArrayList<Potez> potezi) {
        this.potezi = potezi;
        mojaKarta1.setVisible(false);
        mojaKarta2.setVisible(false);
        mojaKarta3.setVisible(false);
        adut.setVisible(false);
        mac.setVisible(false);
        odigranaDonjaKarta.setVisible(false);
        odigranaGornjaKarta.setVisible(false);
        pobjedaRuke.setVisible(false);

        Thread t1 = new Thread(new PokreniReplayThread(potezi));
        t1.start();
    }

    private void pokreniReplay() throws InterruptedException {
        System.out.println("replay pocinje");
        for (Potez potez : potezi) {
            int resultBacanja = postaviKartuNaStol(potez.getKarta());

            Platform.runLater(()-> {
                setPobjedaRukeText(String.format("%s je bacio %d %s", potez.getIgrac().getNadimak(), potez.getKarta().getBroj(), potez.getKarta().getZog().name()));
            });
                Thread.sleep(350);
                pobjedaRuke.setVisible(true);
                Thread.sleep(850);
                pobjedaRuke.setVisible(false);
                Thread.sleep(500);


            if (resultBacanja == 2){
                odigranaDonjaKarta.setVisible(false);
                odigranaGornjaKarta.setVisible(false);
                Thread.sleep(700);
            }
            System.out.println(potez);
        }
        Platform.runLater(()-> {
            setPobjedaRukeText("Runda završena sa ukupno poteza: " + potezi.size());
        });
        pobjedaRuke.setVisible(true);

    }

    private class PokreniReplayThread implements Runnable {
        private final ArrayList<Potez> potezi;

        public PokreniReplayThread(ArrayList<Potez> potezi) {
            this.potezi = potezi;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(1500);
                pokreniReplay();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public Karta vuciKartu() {
        Karta dummyKarta = new Karta(-1, Zog.Bastoni);
        cc.writeToServer(dummyKarta);
        return (Karta) cc.readFromServer();
    }

    private void pokreniThreadove() {
        DaemonThreadFactory dtf = new DaemonThreadFactory();
        ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor(dtf);
        exec.scheduleAtFixedRate(new PreostaloVrijemeThread(this), 0, 1, TimeUnit.SECONDS);

        DaemonThreadFactory dtf2 = new DaemonThreadFactory();
        ScheduledExecutorService exec2 = Executors.newSingleThreadScheduledExecutor(dtf2);
        exec2.scheduleAtFixedRate(new ProvjeraJeliIgraGotovaThread(this), 1, 1, TimeUnit.SECONDS);
    }

    public void startajRuku() {
        cc.writeToServer(new IgracNaRedu());
        Korisnik _korisnik = (Korisnik) cc.readFromServer();
        System.out.println(_korisnik);
        if (_korisnik.getNadimak().equals(korisnik.getNadimak())) {
            enableMojeKarte();
            System.out.println("sad sam ja na redu...");
        } else {
            System.out.println("Cekam da protivnik baci kartu..");

            disableMojeKarte();
            cekajNaBacanjeKarte();
        }
    }

    private void cekajNaBacanjeKarte() {
        Thread t1 = new Thread(new CekajNaBacanjeKarte());
        t1.setDaemon(true);
        t1.start();
    }

    private class CekajNaBacanjeKarte implements Runnable {
        @Override
        public synchronized void run() {
            boolean flag = true;
            while(flag == true) {
                try{
                    System.out.println("citam....");
                    Object o = cc.readFromServer();
                    Karta k = (Karta) o;
                    System.out.println(k);
                    if (postaviKartuNaStol(k) == 1){
                        enableMojeKarte();
                        break;
                    } else {
                        try {
                            Thread.sleep(30);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        cc.writeToServer(new IgracNaRedu());
                        Korisnik korisnik = (Korisnik)cc.readFromServer();
                        Platform.runLater(() -> doPobjedaRuke(korisnik));
                        System.out.println("kraj ruke");
                        break;
                    }
//                    flag = false;
                }catch(Exception e){}
            }

        }
    }

    private void doKartaClick(MouseEvent event) {
        //dohvacanje pritisnute karte
        ImageView source = (ImageView) event.getSource();
        Karta karta = ((CardImage) source.getImage()).getKarta();

        //slanje na server
        cc.writeToServer(karta);

        //sakrivanje pritisnute karte
        source.setVisible(false);
        disableMojeKarte();

        if (postaviKartuNaStol(karta) == 1){
            disableMojeKarte();
            cekajNaBacanjeKarte();
        } else {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            cc.writeToServer(new IgracNaRedu());
            doPobjedaRuke((Korisnik)cc.readFromServer());
            System.out.println("kraj ruke");
//            podijeliKartu();
        }

    }

    public int postaviKartuNaStol(Karta karta) {
        System.out.println("postavi kartu na stol");
        if (!odigranaDonjaKarta.isVisible()) {
            //ako je 1. karta
            odigranaDonjaKarta.setImage(new Image(karta.getImgPath()));
            odigranaDonjaKarta.setVisible(true);
            return 1;
        } else {
            //ako je 2. karta
            odigranaGornjaKarta.setImage(new Image(karta.getImgPath()));
            odigranaGornjaKarta.setVisible(true);
            return 2;
        }
    }

    private void doPobjedaRuke(Korisnik winner) {
        this.setPobjednikTextLabel(winner);
        //napravi thread koji će pokazati obavijest o pobjedniku
        HideCardsThread hideCardsThread = new HideCardsThread(this);
        hideCardsThread.start();
    }

    public void podijeliKartu() {
        //vuci za AI
        if (getRunda().isProtivnikAI()) {
            getRunda().vuciZaAI();
        }

        //podjela karte (ovisno jel karta1, 2, 3.. itd)

        Karta karta = vuciKartu();
        cc.writeToServer(new Runda());
        runda = ((Runda)cc.readFromServer());
        if (runda.getMac().size() > 0) {
            if (!mojaKarta1.isVisible()) {
                mojaKarta1.setImage(new CardImage(karta.getImgPath(), karta));
                mojaKarta1.setVisible(true);
            } else if (!mojaKarta2.isVisible()) {
                mojaKarta2.setImage(new CardImage(karta.getImgPath(), karta));
                mojaKarta2.setVisible(true);
            } else if (!mojaKarta3.isVisible()) {
                mojaKarta3.setImage(new CardImage(karta.getImgPath(), karta));
                mojaKarta3.setVisible(true);
            }
        }

        //sakrivanje aduta i maca
        if (runda.getMac().size() <= 1) {
            adut.setVisible(false);
            mac.setVisible(false);
        }

        //enableanje / disableanje ruke
        if (runda.getIgracNaRedu() != korisnik) {
            disableMojeKarte();
        } else {
            enableMojeKarte();
        }

        startajRuku();
        if (getRunda().getIgracNaRedu() == Repo.GetKorisnik("AI")) {
            PlayAIThread pat = new PlayAIThread(this);
            pat.start();
        }
    }

    public void disableMojeKarte() {
        mojaKarta1.setDisable(true);
        mojaKarta2.setDisable(true);
        mojaKarta3.setDisable(true);
        mojaKarta1.getStyleClass().remove("imgMyCards");
        mojaKarta2.getStyleClass().remove("imgMyCards");
        mojaKarta3.getStyleClass().remove("imgMyCards");
    }

    public void enableMojeKarte() {
        mojaKarta1.setDisable(false);
        mojaKarta2.setDisable(false);
        mojaKarta3.setDisable(false);
        mojaKarta1.getStyleClass().add("imgMyCards");
        mojaKarta2.getStyleClass().add("imgMyCards");
        mojaKarta3.getStyleClass().add("imgMyCards");

    }

    public void setPobjednikTextLabel(Korisnik winner) {
        pobjedaRuke.setText(winner.getNadimak() + " uzima ruku.");
    }

    public void setPobjedaRukeText(String text) {
        pobjedaRuke.setText(text);
    }


    //----------- Methods -------------

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if (potezi != null){
            System.out.println("potezi.....");
        }
    }

    public void krajRunde() {
        StringBuilder sb = new StringBuilder();
        if (getRunda().getPobjednik() != null) {
            sb.append("Pobjednik je " + getRunda().getPobjednik().getNadimak() + "\n");
        } else {
            sb.append("Nema pobjednika -> Izjednačeno");
        }
        sb.append(getRunda().getIgrac1().getNadimak() + ": " + getRunda().izbrojiPunteIgrac1() + " bodova" + "\n");
        sb.append(getRunda().getIgrac2().getNadimak() + ": " + getRunda().izbrojiPunteIgrac2() + " bodova" + "\n");

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Kraj runde");
        alert.setHeaderText("Kraj runde");
        alert.setContentText(sb.toString());
        alert.showAndWait();
        this.getMojaKarta2().getScene().getWindow().hide();

    }

    //----------- Methods -------------
}

