package briskula.scenes;

import briskula.network.CheckForMessages;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import briskula.dal.Repo;
import briskula.models.Korisnik;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;


public class LoginController implements Initializable {

    @FXML
    private TextField tbLoginNadimak;

    @FXML
    private TextField tbLoginLozinka;

    @FXML
    private TextField tbRegistracijaNadimak;

    @FXML
    private TextField tbRegistracijaEmail;

    @FXML
    private PasswordField tbRegistracijaLozinka;

    @FXML
    private PasswordField tbRegistracijaPonovljena;

    @FXML
    private TextField tbZaboravljenaEmail;

    @FXML
    private TabPane tabPane;


    @FXML
    private void btnPrijavaClick(ActionEvent event) {

        StringBuilder sb = new StringBuilder();

        //errors flas
        boolean hasErrors = false;

        //initially remove all validation styles
        tbLoginNadimak.getStyleClass().remove("error");
        tbLoginLozinka.getStyleClass().remove("error");

        //validate username
        if (tbLoginNadimak.getText().isEmpty()) {
            sb.append("Niste unijeli nadimak!\n");
            tbLoginNadimak.getStyleClass().add("error");
            hasErrors = true;
        }

        //validate password
        if (tbLoginLozinka.getText().isEmpty()) {
            sb.append("Niste unijeli lozinku!\n");
            tbLoginLozinka.getStyleClass().add("error");
            hasErrors = true;
        }

        Korisnik korisnik = Repo.LoginAndGetUser(tbLoginNadimak.getText(), tbLoginLozinka.getText());
        if (korisnik != null){
            try {
                ((Node) (event.getSource())).getScene().getWindow().hide();

                FXMLLoader loader = new FXMLLoader(getClass().getResource("PocetnaStranica.fxml"));
                BorderPane root = loader.load();
                PocetnaStranicaController psc = loader.getController();
                psc.initData(korisnik);

                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setOnCloseRequest(x -> Platform.exit());

                Image applicationIcon = new Image(getClass().getResourceAsStream("../../materials/img/favicon.png"));
                stage.getIcons().add(applicationIcon);
                stage.setTitle("Briškula");

                stage.setScene(scene);
                stage.show();

            } catch (IOException ex) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        else {
            sb.append("Neispravan email ili lozinka!\n");
            hasErrors = true;
        }

        if (hasErrors) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Pogreška kod unošenja podataka!");
            alert.setHeaderText("Neispravni podaci!");
            alert.setContentText(sb.toString());
            alert.showAndWait();
        }

    }

    @FXML
    private void btnRegistracijaClick(ActionEvent event) {

        StringBuilder sb = new StringBuilder();

        //errors flas
        boolean hasErrors = false;

        //initially remove all validation styles
        tbRegistracijaNadimak.getStyleClass().remove("error");
        tbRegistracijaEmail.getStyleClass().remove("error");
        tbRegistracijaLozinka.getStyleClass().remove("error");
        tbRegistracijaPonovljena.getStyleClass().remove("error");

        //validation
        if (tbRegistracijaNadimak.getText().isEmpty()) {
            sb.append("Niste unijeli nadimak!\n");
            tbRegistracijaNadimak.getStyleClass().add("error");
            hasErrors = true;
        }

        if (tbRegistracijaEmail.getText().isEmpty()) {
            sb.append("Niste unijeli email!\n");
            tbRegistracijaEmail.getStyleClass().add("error");
            hasErrors = true;
        }

        if (tbRegistracijaLozinka.getText().isEmpty()) {
            sb.append("Niste unijeli lozinku!\n");
            tbRegistracijaLozinka.getStyleClass().add("error");
            hasErrors = true;
        }

        if (tbRegistracijaPonovljena.getText().isEmpty()) {
            sb.append("Niste unijeli ponovljenu lozinku!\n");
            tbRegistracijaPonovljena.getStyleClass().add("error");
            hasErrors = true;
        }

        if (!tbRegistracijaPonovljena.getText().equals(tbRegistracijaLozinka.getText())) {
            sb.append("Unesene lozinke se ne podudaraju!\n");
            tbRegistracijaLozinka.getStyleClass().add("error");
            tbRegistracijaPonovljena.getStyleClass().add("error");
            hasErrors = true;
        }

        if (hasErrors == false){ //if validation is okay
            Korisnik korisnik = new Korisnik(tbRegistracijaNadimak.getText(), tbRegistracijaEmail.getText(), tbRegistracijaLozinka.getText(), 0, 0, "../../materials/img/user_images/user1.png");

            if (Repo.AddKorisnik(korisnik)){
            SingleSelectionModel<Tab> selectionModel = tabPane.getSelectionModel();
            selectionModel.select(0); //select by index starting with 0
            tbLoginNadimak.setText(tbRegistracijaNadimak.getText());
            tbLoginLozinka.requestFocus();

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Registracija uspješna!");
            alert.setHeaderText("Registracija uspješna!");
            alert.setContentText("Uspješno ste se registrirali u sustav. Možete se prijaviti sa svojim nadimkom i lozinkom.");
            alert.showAndWait();
            }
            else {
                sb.append("Korisnik s tom email adresom već postoji!\n");
                hasErrors = true;

            }

        }

        if (hasErrors) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Pogreška kod unošenja podataka!");
            alert.setHeaderText("Neispravni podaci!");
            alert.setContentText(sb.toString());
            alert.showAndWait();
        }

    }

    @FXML
    private void btnZaboravljenaClick(ActionEvent event) {

        StringBuilder sb = new StringBuilder();

        boolean hasErrors = false;

        //initially remove all validation styles
        tbZaboravljenaEmail.getStyleClass().remove("error");

        //validation
        if (tbZaboravljenaEmail.getText().isEmpty()) {
            sb.append("Niste unijeli Email!\n");
            tbZaboravljenaEmail.getStyleClass().add("error");
            hasErrors = true;
        }

        if (hasErrors) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Pogreška kod unošenja podataka!");
            alert.setHeaderText("Neispravni podaci!");
            alert.setContentText(sb.toString());
            alert.showAndWait();
        }
        else { //if validation is okay
            Repo.PosaljiZaboravljenuLozinku(tbZaboravljenaEmail.getText());

            SingleSelectionModel<Tab> selectionModel = tabPane.getSelectionModel();
            selectionModel.select(0); //select by index starting with 0

            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Zahtjev uspješno poslan!");
            alert.setHeaderText("Zahtjev uspješno poslan!");
            alert.setContentText("Ukoliko korisnik s unesenom email adresom postoji, na email adresu će stići link za resetiranje lozinke.");
            alert.showAndWait();
        }

    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}