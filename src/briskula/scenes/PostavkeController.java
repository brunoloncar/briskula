package briskula.scenes;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import briskula.DocumentationFactory;
import briskula.models.Korisnik;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


public class PostavkeController implements Initializable {

    @FXML
    private TextField tbNadimak;

    @FXML
    private TextField tbEmail;

    @FXML
    private TextField tbLozinka;

    @FXML
    private TextField tbPonovljenaLozinka;

    @FXML
    private TextField tbStaraLozinka;

    @FXML
    private ImageView imgUserAvatar;


    private Korisnik korisnik;

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
        System.out.println("set");
    }

    @FXML
    private void biranjeAvataraClick(ActionEvent event) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("BiranjeAvatara.fxml"));
            Scene scene = new Scene(root);

            Stage stage = new Stage();
            Image applicationIcon = new Image(getClass().getResourceAsStream("../../materials/img/favicon.png"));

            stage.getIcons().add(applicationIcon);
            stage.setTitle("Izmjena avatara");
            stage.setScene(scene);
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void ispisiDokumentacijuClick(ActionEvent event) {
        try {
            String path = DocumentationFactory.GenerateDocumentation();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Uspjeh!");
            alert.setHeaderText("Uspjeh!");
            alert.setContentText("Dokumentacija je uspješno ispisana na putanju: " + path);
            alert.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    public void initData(Korisnik korisnik) {
        if (korisnik != null){
            tbNadimak.setText(korisnik.getNadimak());
            tbEmail.setText(korisnik.getEmail());
            imgUserAvatar.setImage(new Image(getClass().getResourceAsStream(korisnik.getAvatarURL())));
        }
    }
}
