package briskula.scenes;

import briskula.models.Korisnik;
import briskula.models.Runda;
import briskula.network.ChatClient;
import briskula.network.ChatServer;
import briskula.network.ChatService;
import briskula.network.Server;
import briskula.threads.SendChatMessageThread;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.util.ResourceBundle;


public class ChatController implements Initializable {

    private boolean amIServer = false;

    @FXML
    private ImageView protivnikIcon;

    @FXML
    private Label protivnikName;

    @FXML
    private TextArea chatBox;

    @FXML
    private TextField chatMessage;
    private Korisnik korisnik;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void initData(Korisnik korisnik, Korisnik protivnik) {
        this.korisnik = korisnik;
        protivnikName.setText(protivnik.getNadimak());
        protivnikIcon.setImage(new Image(getClass().getResourceAsStream(korisnik.getAvatarURL())));
        try {
            try {
                ChatServer.runServer(korisnik.getNadimak(), chatBox);
                amIServer = true;
            } catch (Exception e) {
                ChatClient.runClient(korisnik.getNadimak(), chatBox);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void posaljiClick(ActionEvent event) {
        try {
            String message = "[" + korisnik.getNadimak() + "]: " + chatMessage.getText();
            chatMessage.setText("");
            chatMessage.requestFocus();
            chatBox.appendText(message + "\n");
            new SendChatMessageThread(amIServer, message).start();

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
