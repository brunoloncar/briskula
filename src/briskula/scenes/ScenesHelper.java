package briskula.scenes;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ScenesHelper {

    public static void CreateAndShowScreen(String ScreenName, String title, ActionEvent event){
        Parent root;
        try {
            root = FXMLLoader.load(ScenesHelper.class.getResource(ScreenName));
            Scene scene = new Scene(root);

            Stage stage = new Stage();
            Image applicationIcon = new Image(ScenesHelper.class.getResourceAsStream("../../materials/img/favicon.png"));
            stage.getIcons().add(applicationIcon);
            stage.setTitle(title);
            stage.setScene(scene);
            stage.show();

        } catch (IOException ex) {
            Logger.getLogger(ScenesHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void CreateAndHideOldScreen(String ScreenName, String title, ActionEvent event){
        Parent root;
        try {
            root = FXMLLoader.load(ScenesHelper.class.getResource(ScreenName));
            Scene scene = new Scene(root);

            Stage stage = new Stage();
            Image applicationIcon = new Image(ScenesHelper.class.getResourceAsStream("../../materials/img/favicon.png"));
            stage.getIcons().add(applicationIcon);
            stage.setTitle(title);
            stage.setScene(scene);
            stage.show();
            ((Node) (event.getSource())).getScene().getWindow().hide();

        } catch (IOException ex) {
            Logger.getLogger(ScenesHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
