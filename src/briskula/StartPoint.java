package briskula;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

public class StartPoint extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {

        //Login
        Parent root = FXMLLoader.load(getClass().getResource("scenes/Login.fxml"));
        primaryStage.setTitle("Briškula");
        Scene scene = new Scene(root, 404, 360);

        Image applicationIcon = new Image(getClass().getResourceAsStream("../materials/img/favicon.png"));
        primaryStage.getIcons().add(applicationIcon);

        scene.getStylesheets().add(getClass().getResource("../materials/mainStyle.css").toExternalForm()); //add stylesheet class
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }
}
